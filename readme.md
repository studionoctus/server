# CHAT APP - Server

uses Laravel 5.2

- app\Http\routes.php

```
#!php

Route::post('messages', 'PusherController@messages');
```

- app\Http\Controllers\PusherController@messages

```
#!php

Pusher::trigger('messages', 'new_message',
	[
	'text' => $request->input('text'),
	'name' => $request->input('name'),
	'time' => $request->input('time'),
	]
);

return response()->json(['success' => 200]);
```

- app/Http/Kernel.php

**VerifyCsrfToken** on web middleware disabled.