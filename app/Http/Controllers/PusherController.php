<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use LaravelPusher as Pusher;
use Log;

class PusherController extends Controller
{
    public function messages(Request $request)
    {
        // Log::debug($request);
        
        Pusher::trigger('messages', 'new_message',
            [
                'text' => $request->input('text'),
                'name' => $request->input('name'),
                'time' => $request->input('time'),
            ]
        );
        
        return response()->json(['success' => 200]);
    }
}
